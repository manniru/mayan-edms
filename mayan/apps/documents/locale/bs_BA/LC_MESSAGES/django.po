# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Atdhe Tabaku <Atdhe617@gmail.com>, 2018
# Ilvana Dollaroviq <ilvanadollaroviq@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:43-0400\n"
"PO-Revision-Date: 2018-09-27 02:30+0000\n"
"Last-Translator: Atdhe Tabaku <Atdhe617@gmail.com>\n"
"Language-Team: Bosnian (Bosnia and Herzegovina) (http://www.transifex.com/rosarior/mayan-edms/language/bs_BA/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bs_BA\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: apps.py:115 apps.py:268 events.py:7 menus.py:10 models.py:238
#: permissions.py:7 queues.py:18 settings.py:12 statistics.py:231
msgid "Documents"
msgstr "Dokumenti"

#: apps.py:136
msgid "Create a document type"
msgstr "Kreirajte tip dokumenta"

#: apps.py:138
msgid ""
"Every uploaded document must be assigned a document type, it is the basic "
"way Mayan EDMS categorizes documents."
msgstr "Svaki uploadovani dokument mora biti dodeljen tipu dokumenta, to je osnovni način koji Maian EDMS kategorizuje dokumente."

#: apps.py:157
msgid "Versions comment"
msgstr ""

#: apps.py:160
msgid "Versions encoding"
msgstr ""

#: apps.py:163
msgid "Versions mime type"
msgstr ""

#: apps.py:166
msgid "Versions timestamp"
msgstr ""

#: apps.py:231 apps.py:248 apps.py:255 apps.py:283 apps.py:298 apps.py:324
msgid "Thumbnail"
msgstr "Thumbnail"

#: apps.py:240 apps.py:307 forms.py:186 links.py:84
msgid "Pages"
msgstr "Stranice"

#: apps.py:262
msgid "Type"
msgstr "Tip"

#: apps.py:275 models.py:769
msgid "Enabled"
msgstr "Omogućeno"

#: apps.py:330 links.py:366 views/document_views.py:846
msgid "Duplicates"
msgstr "Duplikati"

#: dashboard_widgets.py:24
msgid "Total pages"
msgstr ""

#: dashboard_widgets.py:46
msgid "Total documents"
msgstr "Ukupni dokumenti"

#: dashboard_widgets.py:65 views/document_views.py:168
msgid "Documents in trash"
msgstr "Dokumenti u smeću"

#: dashboard_widgets.py:84 links.py:352 links.py:357 permissions.py:55
#: views/document_type_views.py:71
msgid "Document types"
msgstr "Tipovi dokumenta"

#: dashboard_widgets.py:103
msgid "New documents this month"
msgstr "Novi dokumenti ovog meseca"

#: dashboard_widgets.py:116
msgid "New pages this month"
msgstr "Nove stranice ovog meseca"

#: events.py:10
msgid "Document created"
msgstr "Kreirani dokument"

#: events.py:13
msgid "Document downloaded"
msgstr "Dokument je preuzet"

#: events.py:16
msgid "New version uploaded"
msgstr "Nova verzija je otpremljena"

#: events.py:19
msgid "Document properties edited"
msgstr "Karakteristike dokumenata izmenjene"

#: events.py:23
msgid "Document type changed"
msgstr "Tip dokumenta promenjen"

#: events.py:27
msgid "Document type created"
msgstr ""

#: events.py:31
msgid "Document type edited"
msgstr ""

#: events.py:34
msgid "Document version reverted"
msgstr "Verzija dokumenta se povukla"

#: events.py:37
msgid "Document viewed"
msgstr "Dokument je pregledan"

#: forms.py:96
msgid "Quick document rename"
msgstr "Brzo preimenuj dokument"

#: forms.py:104 forms.py:256
msgid "Preserve extension"
msgstr "Sačuvaj produžetak"

#: forms.py:106
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open document correctly."
msgstr ""

#: forms.py:149
msgid "Date added"
msgstr "Datum dodavanja"

#: forms.py:153 models.py:185
msgid "UUID"
msgstr "UUID"

#: forms.py:155 models.py:209
msgid "Language"
msgstr "Jezik"

#: forms.py:157
msgid "Unknown"
msgstr "Nepoznat"

#: forms.py:165
msgid "File mimetype"
msgstr "Mimetype datoteke"

#: forms.py:166 forms.py:171
msgid "None"
msgstr "Nijedno"

#: forms.py:169
msgid "File encoding"
msgstr "Kodiranje datoteka"

#: forms.py:175 models.py:1018
msgid "File size"
msgstr "Veličina datoteke"

#: forms.py:180
msgid "Exists in storage"
msgstr "Postoji u pohrani"

#: forms.py:182
msgid "File path in storage"
msgstr "Putanja dokumenta u pohrani"

#: forms.py:185 models.py:464 search.py:24 search.py:48
msgid "Checksum"
msgstr "Checksum"

#: forms.py:213 models.py:103 models.py:189 models.py:764 search.py:16
#: search.py:35
msgid "Document type"
msgstr "Tip dokumenta"

#: forms.py:229
msgid "Compress"
msgstr "Kompresuj"

#: forms.py:231
msgid ""
"Download the document in the original format or in a compressed manner. This"
" option is selectable only when downloading one document, for multiple "
"documents, the bundle will always be downloads as a compressed file."
msgstr "Preuzmite dokument u originalnom formatu ili na kompresivan način. Ova opcija se može izabrati samo prilikom preuzimanja jednog dokumenta, za više dokumenata, paket će uvek biti preuzet kao komprimirana datoteka."

#: forms.py:238
msgid "Compressed filename"
msgstr "Naziv kompresovane datoteke"

#: forms.py:241
msgid ""
"The filename of the compressed file that will contain the documents to be "
"downloaded, if the previous option is selected."
msgstr "Naziv kompresovane datoteke koji sadrži dokumente koji su za download, ako je ova opcija odabrana."

#: forms.py:258
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open the downloaded "
"document version correctly."
msgstr "Uzima produžetak datoteke i pomera je do kraja imena datoteke koja omogućava operativne sisteme koji se oslanjaju na produžetak datoteka da bi ispravno otvorili preuzetu verziju dokumenta."

#: forms.py:270 literals.py:39
msgid "Page range"
msgstr "Opseg stranice"

#: forms.py:276
msgid ""
"Page number from which all the transformation will be cloned. Existing "
"transformations will be lost."
msgstr "Broj stranice sa koje će se sve transformacije klonirati. Postojeće transformacije će biti izgubljene."

#: links.py:70
msgid "Preview"
msgstr "Pregled"

#: links.py:75
msgid "Properties"
msgstr "Osobine"

#: links.py:80 links.py:200
msgid "Versions"
msgstr "Verzije"

#: links.py:92 links.py:152
msgid "Clear transformations"
msgstr "Izbriši transformacije"

#: links.py:97
msgid "Clone transformations"
msgstr "Kloniraj transformacije"

#: links.py:102 links.py:160 links.py:325 links.py:340
msgid "Delete"
msgstr "Obriši"

#: links.py:106 links.py:164
msgid "Add to favorites"
msgstr ""

#: links.py:111 links.py:168
msgid "Remove from favorites"
msgstr ""

#: links.py:116 links.py:156
msgid "Move to trash"
msgstr "Pošalji u đubre"

#: links.py:122
msgid "Edit properties"
msgstr "Izmenite svojstva"

#: links.py:126 links.py:172
msgid "Change type"
msgstr "Izmjeni tip"

#: links.py:131 links.py:176
msgid "Advanced download"
msgstr ""

#: links.py:135
msgid "Print"
msgstr "Print"

#: links.py:139
msgid "Quick download"
msgstr ""

#: links.py:143 links.py:179
msgid "Recalculate page count"
msgstr "Ponovo izračunajte broj stranica"

#: links.py:147 links.py:183
msgid "Restore"
msgstr "Vratiti"

#: links.py:189
msgid "Download version"
msgstr "Preuzmite verziju"

#: links.py:194 links.py:275 models.py:237 models.py:427 models.py:1052
#: models.py:1082 models.py:1111
msgid "Document"
msgstr "Dokument"

#: links.py:205
msgid "Details"
msgstr "Detalji"

#: links.py:210 views/document_views.py:96
msgid "All documents"
msgstr "Svi dokumenti"

#: links.py:214 views/document_views.py:885
msgid "Favorites"
msgstr ""

#: links.py:218 views/document_views.py:969
msgid "Recently accessed"
msgstr ""

#: links.py:222 views/document_views.py:993
msgid "Recently added"
msgstr ""

#: links.py:226
msgid "Trash can"
msgstr "Kanta za smeće"

#: links.py:234
msgid ""
"Clear the graphics representations used to speed up the documents' display "
"and interactive transformations results."
msgstr "Obriši grafičku prezentaciju korištenu za ubrzanje prikaza dokumenata i interaktivnu transformaciju rezultata."

#: links.py:237
msgid "Clear document image cache"
msgstr "Obrišite cache memorije dokumenta"

#: links.py:241 permissions.py:51
msgid "Empty trash"
msgstr "Prazni smeće"

#: links.py:250
msgid "First page"
msgstr "Prva stranica"

#: links.py:255
msgid "Last page"
msgstr "Zadnja stranica"

#: links.py:263
msgid "Previous page"
msgstr "Prethodna stranica"

#: links.py:269
msgid "Next page"
msgstr "Sledeća stranica"

#: links.py:281
msgid "Rotate left"
msgstr "Rotirajte levo"

#: links.py:286
msgid "Rotate right"
msgstr "Rotirajte desno"

#: links.py:289
msgid "Page image"
msgstr "Slika za stranicu"

#: links.py:293
msgid "Reset view"
msgstr "Resetiraj prikaz"

#: links.py:299
msgid "Zoom in"
msgstr "Uveličaj"

#: links.py:305
msgid "Zoom out"
msgstr "Smanji"

#: links.py:313
msgid "Revert"
msgstr "Vraćaj"

#: links.py:320 views/document_type_views.py:86
msgid "Create document type"
msgstr "Kreirajte tip dokumenta"

#: links.py:329 links.py:345
msgid "Edit"
msgstr "Urediti"

#: links.py:335
msgid "Add quick label to document type"
msgstr "Dodajte brzi nalepnicu u tip dokumenta"

#: links.py:349 models.py:775
msgid "Quick labels"
msgstr "Brze etikete"

#: links.py:361 models.py:1055 models.py:1065 views/document_views.py:865
msgid "Duplicated documents"
msgstr "Duplirani dokumenti"

#: links.py:372
msgid "Duplicated document scan"
msgstr "Duplikat skeniranja dokumenata"

#: literals.py:30
msgid "Default"
msgstr "default"

#: literals.py:39
msgid "All pages"
msgstr "Sve stranice"

#: models.py:74
msgid "The name of the document type."
msgstr ""

#: models.py:75 models.py:193 models.py:767 search.py:21 search.py:42
msgid "Label"
msgstr "Labela"

#: models.py:79
msgid ""
"Amount of time after which documents of this type will be moved to the "
"trash."
msgstr "Količina vremena nakon koje će se dokumenti ove vrste prebaciti u smeće."

#: models.py:81
msgid "Trash time period"
msgstr "Period u smeću"

#: models.py:85
msgid "Trash time unit"
msgstr "Jedinica za otpatke"

#: models.py:89
msgid ""
"Amount of time after which documents of this type in the trash will be "
"deleted."
msgstr "Količina vremena nakon čega će se dokumenti ove vrste u smeću izbrisati."

#: models.py:91
msgid "Delete time period"
msgstr "Obrišite vremenski period"

#: models.py:96
msgid "Delete time unit"
msgstr "Izbrišite vremensku jedinicu"

#: models.py:104
msgid "Documents types"
msgstr "Tipovi dokumenata"

#: models.py:183
msgid ""
"UUID of a document, universally Unique ID. An unique identifiergenerated for"
" each document."
msgstr ""

#: models.py:193
msgid "The name of the document."
msgstr ""

#: models.py:197
msgid "An optional short text describing a document."
msgstr ""

#: models.py:198 search.py:22 search.py:45
msgid "Description"
msgstr "Opis"

#: models.py:202
msgid ""
"The server date and time when the document was finally processed and added "
"to the system."
msgstr ""

#: models.py:204 models.py:1058
msgid "Added"
msgstr "Dodaj"

#: models.py:208
msgid "The dominant language in the document."
msgstr ""

#: models.py:213
msgid "Whether or not this document is in the trash."
msgstr ""

#: models.py:214
msgid "In trash?"
msgstr "U smeće?"

#: models.py:219
msgid "The server date and time when the document was moved to the trash."
msgstr ""

#: models.py:221
msgid "Date and time trashed"
msgstr "Datum i vreme uklonjeni"

#: models.py:225
msgid ""
"A document stub is a document with an entry on the database but no file "
"uploaded. This could be an interrupted upload or a deferred upload via the "
"API."
msgstr "Stub dokumenta je dokument sa unosom u bazu podataka, ali datoteka nije otpremljena. Ovo bi moglo biti prekinuto otpremanje ili odloženo otpremanje preko API-ja."

#: models.py:228
msgid "Is stub?"
msgstr "Da li je stub?"

#: models.py:241
#, python-format
msgid "Document stub, id: %d"
msgstr "Dokument stub, id: %d"

#: models.py:431
msgid "The server date and time when the document version was processed."
msgstr ""

#: models.py:432
msgid "Timestamp"
msgstr "Vremenska oznaka"

#: models.py:436
msgid "An optional short text describing the document version."
msgstr ""

#: models.py:437
msgid "Comment"
msgstr "Komentar"

#: models.py:443
msgid "File"
msgstr "Datoteka"

#: models.py:447
msgid ""
"The document version's file mimetype. MIME types are a standard way to "
"describe the format of a file, in this case the file format of the document."
" Some examples: \"text/plain\" or \"image/jpeg\". "
msgstr ""

#: models.py:451 search.py:19 search.py:39
msgid "MIME type"
msgstr "MIME tip"

#: models.py:455
msgid ""
"The document version file encoding. binary 7-bit, binary 8-bit, text, "
"base64, etc."
msgstr ""

#: models.py:457
msgid "Encoding"
msgstr "Enkoding"

#: models.py:469 models.py:470 models.py:788
msgid "Document version"
msgstr "Verzija dokumenta"

#: models.py:774
msgid "Quick label"
msgstr "Brza etiketa"

#: models.py:792
msgid "Page number"
msgstr "Broj stranice"

#: models.py:799 models.py:1011 models.py:1044
msgid "Document page"
msgstr "Strnica dokumenta"

#: models.py:800 models.py:1045
msgid "Document pages"
msgstr "Dokumenti dokumenta"

#: models.py:804
#, python-format
msgid "Page %(page_num)d out of %(total_pages)d of %(document)s"
msgstr "Stranica %(page_num)d od ukupno %(total_pages)d u %(document)s"

#: models.py:1014
msgid "Date time"
msgstr "Datum vreme"

#: models.py:1016
msgid "Filename"
msgstr "Naziv datoteke"

#: models.py:1024
msgid "Document page cached image"
msgstr "Dokumentna stranica sačuvana slika"

#: models.py:1025
msgid "Document page cached images"
msgstr "Dokumentirane stranice sa dokumentima"

#: models.py:1064
msgid "Duplicated document"
msgstr "Duplirani dokument"

#: models.py:1078 models.py:1107
msgid "User"
msgstr "Korisnik"

#: models.py:1088
msgid "Favorite document"
msgstr ""

#: models.py:1089
msgid "Favorite documents"
msgstr ""

#: models.py:1114
msgid "Accessed"
msgstr "Omogućeno"

#: models.py:1121
msgid "Recent document"
msgstr "Predhodni dokument"

#: models.py:1122
msgid "Recent documents"
msgstr "Nedavni dokumenti"

#: permissions.py:10
msgid "Create documents"
msgstr "Kreiraj dokumente"

#: permissions.py:13
msgid "Delete documents"
msgstr "Obriši dokumente"

#: permissions.py:16
msgid "Trash documents"
msgstr "Dokumenti za otpatke"

#: permissions.py:19 views/document_views.py:502
msgid "Download documents"
msgstr "Download dokumenata"

#: permissions.py:22
msgid "Edit documents"
msgstr "Izmijeni dokument"

#: permissions.py:25
msgid "Create new document versions"
msgstr "Kreiraj novu verziju dokumenta"

#: permissions.py:28
msgid "Edit document properties"
msgstr "Izmijeni osobine dokumenta"

#: permissions.py:31
msgid "Print documents"
msgstr "Štampajte dokumente"

#: permissions.py:34
msgid "Restore trashed document"
msgstr "Obnovite povučeni dokument"

#: permissions.py:37
msgid "Execute document modifying tools"
msgstr "Izvrši alate za izmjenu dokumenta"

#: permissions.py:41
msgid "Revert documents to a previous version"
msgstr "Vrati dokument na prethodnu verziju"

#: permissions.py:45
msgid "View documents' versions list"
msgstr "Pregledajte listu verzija dokumenata"

#: permissions.py:48
msgid "View documents"
msgstr "Pregled dokumenata"

#: permissions.py:58
msgid "Create document types"
msgstr "Kreiraj tipove dokumenata"

#: permissions.py:61
msgid "Delete document types"
msgstr "Obriši tipove dokumenata"

#: permissions.py:64
msgid "Edit document types"
msgstr "Izmijeni tipove dokumenata"

#: permissions.py:67
msgid "View document types"
msgstr "Pregledaj tipove dokumenata"

#: queues.py:9
msgid "Converter"
msgstr "Konverter"

#: queues.py:12
msgid "Documents periodic"
msgstr "Dokumenti periodični"

#: queues.py:15
msgid "Uploads"
msgstr "Uploads"

#: queues.py:23
msgid "Generate document page image"
msgstr "Generišite sliku dokumenta dokumenta"

#: queues.py:28
msgid "Delete a document"
msgstr "Izbrišite dokument"

#: queues.py:32
msgid "Clean empty duplicate lists"
msgstr "Očistite prazne duplirane liste"

#: queues.py:37
msgid "Check document type delete periods"
msgstr "Proverite periode izbrisanja dokumenta"

#: queues.py:41
msgid "Check document type trash periods"
msgstr "Proverite periode otpada dokumenta"

#: queues.py:45
msgid "Delete document stubs"
msgstr "Obrišite ispravke dokumenata"

#: queues.py:50
msgid "Clear image cache"
msgstr "Obrišite keš memorije"

#: queues.py:55
msgid "Update document page count"
msgstr "Ažurirajte broj stranice dokumenta"

#: queues.py:59
msgid "Upload new document version"
msgstr "Otpremite novu verziju dokumenta"

#: settings.py:17
msgid ""
"Path to the Storage subclass to use when storing the cached document image "
"files."
msgstr ""

#: settings.py:26
msgid "Arguments to pass to the DOCUMENT_CACHE_STORAGE_BACKEND."
msgstr ""

#: settings.py:32
msgid ""
"Disables the first cache tier which stores high resolution, non transformed "
"versions of documents's pages."
msgstr "Onemogućava prvu tačku keša koja skladišti visoke rezolucije, nepreformirane verzije dokumenata."

#: settings.py:39
msgid ""
"Disables the second cache tier which stores medium to low resolution, "
"transformed (rotated, zoomed, etc) versions of documents' pages."
msgstr "Onemogućava drugi sloj keša koji čuva medije do niske rezolucije, transformiše (rotira, zumira, itd.) Verzije stranica dokumenata."

#: settings.py:53
msgid "Maximum number of favorite documents to remember per user."
msgstr ""

#: settings.py:59
msgid ""
"Detect the orientation of each of the document's pages and create a "
"corresponding rotation transformation to display it rightside up. This is an"
" experimental feature and it is disabled by default."
msgstr "Otkrijte orijentaciju svake stranice dokumenta i kreirajte odgovarajuću transformaciju rotacije da biste je prikazali. Ovo je eksperimentalna funkcija i ona je podrazumevano onemogućena."

#: settings.py:67
msgid "Default documents language (in ISO639-3 format)."
msgstr "Podrazumevani jezik dokumenta (u formatu ISO639-3)."

#: settings.py:71
msgid "List of supported document languages. In ISO639-3 format."
msgstr "Lista podržanih jezika dokumenata. U formatu ISO639-3."

#: settings.py:76
msgid ""
"Time in seconds that the browser should cache the supplied document images. "
"The default of 31559626 seconds corresponde to 1 year."
msgstr ""

#: settings.py:95
msgid ""
"Maximum number of recently accessed (created, edited, viewed) documents to "
"remember per user."
msgstr ""

#: settings.py:102
msgid "Maximum number of recently created documents to show."
msgstr ""

#: settings.py:108
msgid "Amount in degrees to rotate a document page per user interaction."
msgstr "Iznos u stepenima za rotaciju stranice dokumenta po korisniku."

#: settings.py:114
msgid "Path to the Storage subclass to use when storing document files."
msgstr ""

#: settings.py:122
msgid "Arguments to pass to the DOCUMENT_STORAGE_BACKEND."
msgstr ""

#: settings.py:126
msgid "Height in pixels of the document thumbnail image."
msgstr ""

#: settings.py:137
msgid ""
"Maximum amount in percent (%) to allow user to zoom in a document page "
"interactively."
msgstr "Maksimalni dozvoljeni iznos u procentima (%)  korisniku da zumira stranicu dokumenta."

#: settings.py:144
msgid ""
"Minimum amount in percent (%) to allow user to zoom out a document page "
"interactively."
msgstr "Minimalni dozvoljeni iznos u procentima (%) po korisniku da zumira stranicu dokumenta."

#: settings.py:151
msgid "Amount in percent zoom in or out a document page per user interaction."
msgstr "Iznos u procentima zumiranja stranice dokumenta po korisničkoj sesiji."

#: statistics.py:16
msgid "January"
msgstr "Januar"

#: statistics.py:16
msgid "February"
msgstr "Februar"

#: statistics.py:16
msgid "March"
msgstr "Mart"

#: statistics.py:16
msgid "April"
msgstr "April"

#: statistics.py:16
msgid "May"
msgstr "Maj"

#: statistics.py:17
msgid "June"
msgstr "Jun"

#: statistics.py:17
msgid "July"
msgstr "Juli"

#: statistics.py:17
msgid "August"
msgstr "Avgust"

#: statistics.py:17
msgid "September"
msgstr "Septembar"

#: statistics.py:17
msgid "October"
msgstr "Oktobar"

#: statistics.py:18
msgid "November"
msgstr "Novembar"

#: statistics.py:18
msgid "December"
msgstr "Decembar"

#: statistics.py:235
msgid "New documents per month"
msgstr "Novi dokumenti mesečno"

#: statistics.py:242
msgid "New document versions per month"
msgstr "Nove verzije dokumenta mesečno"

#: statistics.py:249
msgid "New document pages per month"
msgstr "Nove stranice dokumenta mesečno"

#: statistics.py:256
msgid "Total documents at each month"
msgstr "Ukupni dokumenti svakog meseca"

#: statistics.py:263
msgid "Total document versions at each month"
msgstr "Ukupne verzije dokumenata svakog meseca"

#: statistics.py:270
msgid "Total document pages at each month"
msgstr "Ukupne stranice dokumenta svakog meseca"

#: templates/documents/forms/widgets/document_page_carousel.html:16
#, python-format
msgid ""
"\n"
"                    Page %(page_number)s of %(total_pages)s\n"
"                "
msgstr "\n                    Stranica %(page_number)s od %(total_pages)s\n                "

#: templates/documents/forms/widgets/document_page_carousel.html:22
msgid "No pages to display"
msgstr "Nema stranica za prikazivanje"

#: views/document_page_views.py:49
#, python-format
msgid "Pages for document: %s"
msgstr "Stranice za dokument: %s"

#: views/document_page_views.py:104
msgid "Unknown view keyword argument schema, unable to redirect."
msgstr "Nepoznati prikaz schema argumenta ključne riječi, ne može se preusmjeriti."

#: views/document_page_views.py:136
msgid "There are no more pages in this document"
msgstr "Ovaj dokument nema više stranica."

#: views/document_page_views.py:153
msgid "You are already at the first page of this document"
msgstr "Već ste na prvoj stranici ovog dokumenta"

#: views/document_page_views.py:181
#, python-format
msgid "Image of: %s"
msgstr "Slika od: %s"

#: views/document_type_views.py:46
#, python-format
msgid "Documents of type: %s"
msgstr "Dokumenti tipa: %s"

#: views/document_type_views.py:64
msgid ""
"Document types are the most basic units of configuration. Everything in the "
"system will depend on them. Define a document type for each type of physical"
" document you intend to upload. Example document types: invoice, receipt, "
"manual, prescription, balance sheet."
msgstr ""

#: views/document_type_views.py:70
msgid "No document types available"
msgstr ""

#: views/document_type_views.py:102
msgid "All documents of this type will be deleted too."
msgstr "Svi dokumenti ovog tipa takođe će biti izbrisani."

#: views/document_type_views.py:104
#, python-format
msgid "Delete the document type: %s?"
msgstr "Izbrišite tip dokumenta: %s?"

#: views/document_type_views.py:120
#, python-format
msgid "Edit document type: %s"
msgstr "Izmeni tip dokumenta: %s"

#: views/document_type_views.py:150
#, python-format
msgid "Create quick label for document type: %s"
msgstr "Kreirajte brzu etiketu za tip dokumenta: %s"

#: views/document_type_views.py:171
#, python-format
msgid "Edit quick label \"%(filename)s\" from document type \"%(document_type)s\""
msgstr "Izmenite brzi znak \"%(filename)s\" iz dokumenta tipa \"%(document_type)s\""

#: views/document_type_views.py:196
#, python-format
msgid ""
"Delete the quick label: %(label)s, from document type \"%(document_type)s\"?"
msgstr "Izbrišite brzu etiketu: %(label)s, iz dokumenta tipa \"%(document_type)s\"?"

#: views/document_type_views.py:232
msgid ""
"Quick labels are predetermined filenames that allow the quick renaming of "
"documents as they are uploaded by selecting them from a list. Quick labels "
"can also be used after the documents have been uploaded."
msgstr ""

#: views/document_type_views.py:238
msgid "There are no quick labels for this document type"
msgstr ""

#: views/document_type_views.py:241
#, python-format
msgid "Quick labels for document type: %s"
msgstr "Brze etikete za tip dokumenta: %s"

#: views/document_version_views.py:48
#, python-format
msgid "Versions of document: %s"
msgstr "Verzije dokumenta: %s"

#: views/document_version_views.py:62
msgid "All later version after this one will be deleted too."
msgstr "Sve naknadne verzije nakon ove će također biti obrisane."

#: views/document_version_views.py:65
msgid "Revert to this version?"
msgstr "Vratite se na ovu verziju?"

#: views/document_version_views.py:75
msgid "Document version reverted successfully"
msgstr "Verzija dokumenta uspješno vraćena"

#: views/document_version_views.py:80
#, python-format
msgid "Error reverting document version; %s"
msgstr "Greška pri vraćanju verzije dokumenta; %s"

#: views/document_version_views.py:99
msgid "Download document version"
msgstr "Preuzmite verziju dokumenta"

#: views/document_version_views.py:165
#, python-format
msgid "Preview of document version: %s"
msgstr "Pregled verzije dokumenta: %s"

#: views/document_views.py:71
#, python-format
msgid "Error retrieving document list: %(exception)s."
msgstr ""

#: views/document_views.py:91
msgid ""
"This could mean that no documents have been uploaded or that your user "
"account has not been granted the view permission for any document or "
"document type."
msgstr ""

#: views/document_views.py:95
msgid "No documents available"
msgstr ""

#: views/document_views.py:109
msgid "Delete the selected document?"
msgstr "Izbrišite izabrani dokument?"

#: views/document_views.py:130
#, python-format
msgid "Document: %(document)s deleted."
msgstr "Dokument: %(document)s je izbrisan."

#: views/document_views.py:138
msgid "Delete the selected documents?"
msgstr "Obrišite izabrane dokumente?"

#: views/document_views.py:161
msgid ""
"To avoid loss of data, documents are not deleted instantly. First, they are "
"placed in the trash can. From here they can be then finally deleted or "
"restored."
msgstr ""

#: views/document_views.py:166
msgid "There are no documents in the trash can"
msgstr ""

#: views/document_views.py:179
#, python-format
msgid "Document type change request performed on %(count)d document"
msgstr "Zahtev za izmenu tipa dokumenta izvršen na dokumentu %(count)d"

#: views/document_views.py:182
#, python-format
msgid "Document type change request performed on %(count)d documents"
msgstr "Zahtev za izmenu vrste dokumenta izvršen na dokumentima %(count)d"

#: views/document_views.py:189
msgid "Change"
msgstr "Promeni"

#: views/document_views.py:191
msgid "Change the type of the selected document"
msgid_plural "Change the type of the selected documents"
msgstr[0] "Promenite vrstu izabranog dokumenta"
msgstr[1] "Promenite vrstu izabranih dokumenata"
msgstr[2] "Promenite vrstu izabranih dokumenata"

#: views/document_views.py:202
#, python-format
msgid "Change the type of the document: %s"
msgstr "Promijenite vrstu dokumenta: %s"

#: views/document_views.py:223
#, python-format
msgid "Document type for \"%s\" changed successfully."
msgstr "Tip dokumenta za \"%s\" se uspešno promjenio."

#: views/document_views.py:248
msgid "Only exact copies of this document will be shown in the this list."
msgstr ""

#: views/document_views.py:252
msgid "There are no duplicates for this document"
msgstr ""

#: views/document_views.py:255
#, python-format
msgid "Duplicates for document: %s"
msgstr "Duplikati za dokument: %s"

#: views/document_views.py:284
#, python-format
msgid "Edit properties of document: %s"
msgstr "Izmenite svojstva dokumenta: %s"

#: views/document_views.py:318
#, python-format
msgid "Preview of document: %s"
msgstr "Pregled dokumenta: %s"

#: views/document_views.py:324
msgid "Restore the selected document?"
msgstr "Vratite izabrani dokument?"

#: views/document_views.py:345
#, python-format
msgid "Document: %(document)s restored."
msgstr "Dokument: %(document)s restauriran."

#: views/document_views.py:353
msgid "Restore the selected documents?"
msgstr "Vratite izabrane dokumente?"

#: views/document_views.py:364
#, python-format
msgid "Move \"%s\" to the trash?"
msgstr "Pomerite \"%s\" u smeće?"

#: views/document_views.py:387
#, python-format
msgid "Document: %(document)s moved to trash successfully."
msgstr "Dokument: %(document)s je uspešno prebačen u smeće."

#: views/document_views.py:400
msgid "Move the selected documents to the trash?"
msgstr "Pomerite odabrane dokumente u smeće?"

#: views/document_views.py:418
#, python-format
msgid "Properties for document: %s"
msgstr "Osobine za dokument: %s"

#: views/document_views.py:424
msgid "Empty trash?"
msgstr "Prazno smeće?"

#: views/document_views.py:437
msgid "Trash emptied successfully"
msgstr "Smeće ispraznjen uspješno"

#: views/document_views.py:500
msgid "Download"
msgstr "Download"

#: views/document_views.py:606
#, python-format
msgid "%(count)d document queued for page count recalculation"
msgstr "%(count)d dokument stavljen u red za ponovni izračun broja stranica"

#: views/document_views.py:609
#, python-format
msgid "%(count)d documents queued for page count recalculation"
msgstr "%(count)d dokumenti postavljeni redom za ponovni izračun broja stranica"

#: views/document_views.py:617
msgid "Recalculate the page count of the selected document?"
msgid_plural "Recalculate the page count of the selected documents?"
msgstr[0] "Ponovo izračunajte broj stranica izabranog dokumenta?"
msgstr[1] "Ponovo izračunajte broj stranica izabranih dokumenata?"
msgstr[2] "Ponovo izračunajte broj stranica izabranih dokumenata?"

#: views/document_views.py:628
#, python-format
msgid "Recalculate the page count of the document: %s?"
msgstr "Ponovo izračunajte broj stranica dokumenta: %s?"

#: views/document_views.py:644
#, python-format
msgid ""
"Document \"%(document)s\" is empty. Upload at least one document version "
"before attempting to detect the page count."
msgstr ""

#: views/document_views.py:657
#, python-format
msgid "Transformation clear request processed for %(count)d document"
msgstr "Zahtev za transformaciju obrađen za dokument %(count)d"

#: views/document_views.py:660
#, python-format
msgid "Transformation clear request processed for %(count)d documents"
msgstr "Zahtev za transformaciju obrađen za dokumente %(count)d"

#: views/document_views.py:668
msgid "Clear all the page transformations for the selected document?"
msgid_plural "Clear all the page transformations for the selected document?"
msgstr[0] "Obrišite sve transformacije stranice za izabrani dokument?"
msgstr[1] "Obrišite sve transformacije stranice za izabrani dokument?"
msgstr[2] "Obrišite sve transformacije stranice za izabrani dokument?"

#: views/document_views.py:679
#, python-format
msgid "Clear all the page transformations for the document: %s?"
msgstr "Obrišite sve transformacije stranice za dokument: %s?"

#: views/document_views.py:694 views/document_views.py:722
#, python-format
msgid ""
"Error deleting the page transformations for document: %(document)s; "
"%(error)s."
msgstr "Greška brisanja transformacija za dokument: %(document)s; %(error)s."

#: views/document_views.py:730
msgid "Transformations cloned successfully."
msgstr "Transformacije su uspešno klonirane."

#: views/document_views.py:745 views/document_views.py:818
msgid "Submit"
msgstr "Podnijeti"

#: views/document_views.py:747
#, python-format
msgid "Clone page transformations for document: %s"
msgstr "Transformacije stranica klonova za dokument: %s"

#: views/document_views.py:821
#, python-format
msgid "Print: %s"
msgstr "Štampa: %s"

#: views/document_views.py:856
msgid ""
"Duplicates are documents that are composed of the exact same file, down to "
"the last byte. Files that have the same text or OCR but are not identical or"
" were saved using a different file format will not appear as duplicates."
msgstr ""

#: views/document_views.py:863
msgid "There are no duplicated documents"
msgstr ""

#: views/document_views.py:881
#, python-format
msgid ""
"Favorited documents will be listed in this view. Up to %(count)d documents "
"can be favorited per user. "
msgstr ""

#: views/document_views.py:884
msgid "There are no favorited documents."
msgstr ""

#: views/document_views.py:895
#, python-format
msgid "%(count)d document added to favorites."
msgstr ""

#: views/document_views.py:898
#, python-format
msgid "%(count)d documents added to favorites."
msgstr ""

#: views/document_views.py:905
msgid "Add"
msgstr "Dodati"

#: views/document_views.py:908
msgid "Add the selected document to favorites"
msgid_plural "Add the selected documents to favorites"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: views/document_views.py:921
#, python-format
msgid "Document \"%(instance)s\" is not in favorites."
msgstr ""

#: views/document_views.py:925
#, python-format
msgid "%(count)d document removed from favorites."
msgstr ""

#: views/document_views.py:928
#, python-format
msgid "%(count)d documents removed from favorites."
msgstr ""

#: views/document_views.py:935
msgid "Remove"
msgstr "Ukloniti"

#: views/document_views.py:938
msgid "Remove the selected document from favorites"
msgid_plural "Remove the selected documents from favorites"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: views/document_views.py:963
msgid ""
"This view will list the latest documents viewed or manipulated in any way by"
" this user account."
msgstr ""

#: views/document_views.py:967
msgid "There are no recently accessed document"
msgstr ""

#: views/document_views.py:987
msgid "This view will list the latest documents uploaded in the system."
msgstr ""

#: views/document_views.py:991
msgid "There are no recently added document"
msgstr ""

#: views/misc_views.py:18
msgid "Clear the document image cache?"
msgstr "Obrišite keš memorije dokumenta?"

#: views/misc_views.py:25
msgid "Document cache clearing queued successfully."
msgstr "Uspjesno izbrisanje cache dokumenta "

#: views/misc_views.py:31
msgid "Scan for duplicated documents?"
msgstr "Skeniraj za duplirana dokumenta?"

#: views/misc_views.py:38
msgid "Duplicated document scan queued successfully."
msgstr "Duplirano skeniranje dokumenata je u redu."

#: widgets.py:81 widgets.py:85
#, python-format
msgid "Pages: %d"
msgstr "Stranice: %d"
